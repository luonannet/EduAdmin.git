/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const webRouter = {
  path: '/web',
  component: Layout,
  redirect: 'noRedirect',
  name: 'web',
  meta: {
    title: 'web',
    icon: 'international'
  },
  children: [
    {
      path: 'template/1',
      component: () => import('@/views/platform/template'),
      name: 'template',
      meta: { title: 'template', icon: 'tree' }
    },
    {
      path: 'news/1',
      name: 'newsList',
      component: () => import('@/views/platform/web/news'),
      meta: { title: 'news', icon: "news" }
    },
    {
      path: 'party/1',
      name: 'party',
      component: () => import('@/views/platform/party'),
      meta: { title: 'party', icon: "wechat" }
    }, 
    // {
    //   path: 'docDownload',
    //   name: 'docDownload',
    //   component: () => import('@/views/platform/web/docDownload'),
    //   meta: { title: 'docDownload' ,icon:"download"}
    // },
    // {
    //   path: 'news',
    //   name: 'newsList',
    //   component: () => import('@/views/platform/web/news'),
    //   meta: { title: 'news' ,icon:"guide"}
    // },
    // {
    //   path: 'jingsai',
    //   name: 'jingsai',
    //   component: () => import('@/views/platform/web/jingsai'),
    //   meta: { title: 'jingsai',icon:"guide" }
    // },
    // {
    //   path: 'teacher',
    //   name: 'teacher',
    //   component: () => import('@/views/platform/web/teacher'),
    //   meta: { title: 'teacher' ,icon:"guide"}
    // }
  ]
}

export default webRouter
