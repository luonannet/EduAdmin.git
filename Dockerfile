FROM nginx
LABEL maintainer="luonancom@qq.com" 
COPY ./build/nginx.conf /etc/nginx/nginx.conf
COPY ./dist /var/www/admin